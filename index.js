//IMPORTS
const Discord = require("discord.js");
const config = require("./config.json");
const axios = require('axios');
const CronJob = require('cron').CronJob;
const command = require('./command.js');
const frasesDiego = require('./frasesDiego.js')

const client = new Discord.Client();

client.on('ready', () => {
    //var generalChannel = client.channels.cache.get("789916107976343592");
    //generalChannel.send('!info para conocer los comandos')

})

client.on('message', message => {

    //informacion de comandos
    if (message.content === '!info') {
        const infoEmded = new Discord.MessageEmbed()
            .addField("Info Comandos", "**!valve**   //   Información sobre CSGO\n\n **!server**   //   Para conocer información sobre servidores br y cl\n\n **!dolar**   //   Información sobre el dolar\n\n **!temperatura mendoza** //   Que temperatura hace en Mendoza\n\n **!temperatura jujuy** //  Que temperatura hace en Jujuy\n\n **!soldano**   //   Gif de perros\n\n **!macri**   //   Gif de gatos\n\n **!diego** // Para gif del DIEEEGOOOTEEEE\n\n **!gg** //  Esta gg el dia?\n\n **!frases diego**  //  Frases Iconicas del DIEGOOTEEEEE\n\n **!simpsons**  //  Frases de los simpsons\n\n **!monas chinas**  // Imagenes Monas chinas");

        message.channel.send({ embed: infoEmded });
    }

    //estado de servidores valve
    if (message.content === '!valve') {
        axios.get('https://api.steampowered.com/ICSGOServers_730/GetGameServersStatus/v1/?key=BCCC7B0B28D7D67AFBEC26F86CCA5EBE'
        )
            .then(function (response) {
                let embedValve = new Discord.MessageEmbed();
                embedValve.addField('**Status Valve**', `SESSIONS LOGON: ${response.data.result.services.SessionsLogon}\n\n PLAYER INVENTORIES: ${response.data.result.services.SteamCommunity}\n\n MATCHMAKING SCHEDULER: ${response.data.result.matchmaking.scheduler}`);
                message.channel.send(embedValve);
            }).catch(e => {
                console.log(e);
            });
    }

    //informacion de servidores sudamerica
    if (message.content === '!server') {
        axios({
            method: 'get',
            url: 'https://api.steampowered.com/ICSGOServers_730/GetGameServersStatus/v1/?key=BCCC7B0B28D7D67AFBEC26F86CCA5EBE'
        })
            .then(function (response) {
                let embedServer = new Discord.MessageEmbed();
                embedServer.addField('**Status Servers**', `SERVIDOR BRASIL -   capacity:   ${response.data.result.datacenters.Brazil.capacity},   load:   ${response.data.result.datacenters.Brazil.load}\n\n SERVIDOR CHILE -   capacity:   ${response.data.result.datacenters.Chile.capacity},   load:   ${response.data.result.datacenters.Chile.load}`);
                message.channel.send(embedServer);
            }).catch(e => {
                console.log(e);
            });
    }

    //informacion cotizacion de dolar
    if (message.content === '!dolar') {
        axios({
            method: 'get',
            url: 'https://api-dolar-argentina.herokuapp.com/api/dolaroficial'
        })
            .then(function (response) {
                let embedDolar = new Discord.MessageEmbed();
                embedDolar.addField('**El Dolar Hoy!!**', `La cotizacion del dolar oficial para la compra en este momento es de: ${response.data.compra}\n\n La cotizacion del dolar oficial para la venta en este momento es de: ${response.data.venta}`);
                message.channel.send(embedDolar);
            }).catch(e => {
                console.log(e);
            });
    }

    //informacion de tiempo mendoza
    if (message.content === '!temperatura mendoza') {
        axios.get('https://api.openweathermap.org/data/2.5/weather?q=Mendoza&appid=963528cf795a3e36df5bc536dded2b6c&units=metric&lang=es')
            .then(response => {
                let embedMza = new Discord.MessageEmbed();
                embedMza.addField("**Tiempo Mza**", `La temperatura de ${response.data.name} en algun momento fue de ${response.data.main.temp} ℃`);
                message.channel.send(embedMza);
            })
            .catch(e => {
                console.log(e);
            });
    }

    //informacion de tiempo jujuy
    if (message.content === '!temperatura jujuy') {
        axios.get('https://api.openweathermap.org/data/2.5/weather?q=san salvador de jujuy&appid=963528cf795a3e36df5bc536dded2b6c&units=metric&lang=es')
            .then(response => {
                let embedujuy = new Discord.MessageEmbed();
                embedujuy.addField("**Tiempo Jujuy**", `La temperatura de ${response.data.name} en algun momento fue de ${response.data.main.temp} ℃`);
                message.channel.send(embedujuy);
            })
            .catch(e => {
                console.log(e);
            });
    }

    //mensaje automatico con cronjob
    const job = new CronJob('14 10 * * *', function () {
        let embedSaludo = new Discord.MessageEmbed();
        embedSaludo.addField(" ", 'Queridos usuarios de Del Moro CHANNEL, les recordamos que los queremos mucho.')
        message.channel.send(embedSaludo);
    }, 'America/Buenos Aires');
    job.start();

    //gif de perros
    if (message.content === '!soldano') {
        axios.get('https://api.giphy.com/v1/gifs/random?api_key=bmvOZPbpuCn6koCOZaLgHskRnW0B8m9I&tag=funny-dogs&rating=g')
            .then(response => {
                message.channel.send(response.data.data.embed_url);
            })
            .catch(e => {
                console.log(e);
            });
    }

    //gif de gatos
    if (message.content === '!macri') {
        axios.get('https://api.giphy.com/v1/gifs/random?api_key=bmvOZPbpuCn6koCOZaLgHskRnW0B8m9I&tag=funny-cats&rating=g')
            .then(response => {
                message.channel.send(response.data.data.embed_url);
            })
            .catch(e => {
                console.log(e);
            });
    }

    //gif de el diegote
    if (message.content === '!diego') {
        axios.get('https://api.giphy.com/v1/gifs/random?api_key=bmvOZPbpuCn6koCOZaLgHskRnW0B8m9I&tag=maradona&rating=g')
            .then(response => {
                message.channel.send(response.data.data.embed_url);
            })
            .catch(e => {
                console.log(e);
            });
    }


    //todavia nada
    if (message.content === '!masterChef') {
        message.channel.send('https://televisionlibre.net/embed/drmplayer.html?get=Ly9jaHJvbWVjYXN0LmN2YXR0di5jb20uYXIvLy8vbGl2ZS9jM2Vkcy9UZWxlZmVIRC9TQV9MaXZlX2Rhc2hfZW5jXzJBL1RlbGVmZUhELm1wZA==');
    }

    //agregar status al bot
    command(client, 'status', (message) => {
        const content = message.content.replace('!status ', '');

        client.user.setPresence({
            activity: {
                name: content,
                type: 0,
            },
        })
    })

    //frases del diego
    if (message.content === "!frases diego") {
        let embedDiego = new Discord.MessageEmbed();
        embedDiego.addField("**Frases del Diegote**", frasesDiego[Math.floor(Math.random() * frasesDiego.length)]);
        message.channel.send(embedDiego);
    }

    //imagenes de monas chinas
    if (message.content === "!monas chinas") {
        axios.get('https://waifu.pics/api/sfw/waifu').then(response => {
            message.channel.send(response.data.url);
        }).catch(e => {
            console.log(e);
        })
    }

    //frases simpsons
    if (message.content === "!simpsons") {
        axios.get('https://los-simpsons-quotes.herokuapp.com/v1/quotes')
            .then(response => {
                let embedSimpsons = new Discord.MessageEmbed();
                embedSimpsons.addField("**Frases de los simpsons**", `${response.data[0].quote}\n\nAutor: ${response.data[0].author}`);
                message.channel.send(embedSimpsons);
            })
            .catch(e => {
                console.log(e);
            });
    }

    //obtener usuarios del servidor
    if (message.content == '!gg') {
        const { memberCount } = message.guild;
        const totalUser = memberCount - 2;

        //let offline = message.guild.members.cache.filter(member => member.presence.status === "offline").size;
        let online = message.guild.members.cache.filter(member => member.presence.status === "online" && member.user.bot === false || member.presence.status === 'idle' || member.presence.status === 'dnd').size;

        if (online === (totalUser) || online === (totalUser - 1)) {
            let embedGG = new Discord.MessageEmbed();
            embedGG.addField('**Esta GG?**', 'Hay equipo completo, y con suplentes para jogar unos tiritos');
            message.channel.send(embedGG);
        } else if (online === (totalUser - 2)) {
            let embedGG1 = new Discord.MessageEmbed();
            embedGG1.addField('**Esta GG?**', 'Hay equipo para el cs muchache pero si se lesiona uno no hay recambio');
            message.channel.send(embedGG1);
        } else if (online === (totalUser - 3)) {
            let embedGG2 = new Discord.MessageEmbed();
            embedGG2.addField('**Esta GG?**', 'Hay CS pero jogamos con un macaco');
            message.channel.send(embedGG2);
        } else if (online === (totalUser - 4)) {
            let embedGG3 = new Discord.MessageEmbed();
            embedGG3.addField('**Esta GG?**', 'Hay quorum para que no nos echen pero jogamos con dos macacos como Luis');
            message.channel.send(embedGG3);
        } else {
            let embedGG4 = new Discord.MessageEmbed();
            embedGG4.addField('**Esta GG?**', 'Este dia esta GG padre');
            message.channel.send(embedGG4);
        }
    }
})

client.login(config.BOT_TOKEN);
